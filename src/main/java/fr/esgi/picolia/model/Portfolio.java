package fr.esgi.picolia.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="PORTFOLIO")
public class Portfolio implements Serializable {
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "USER_ID")
	private User user = new User();

	@OneToMany(cascade = CascadeType.REMOVE, mappedBy = "id")
	private Set<Photo> photos = new HashSet<>();
	
	@NotEmpty
	@Column(name="NOM_PORTFOLIO", nullable=false)
	private String nomPortfolio;
	
	@Column(name="LIKE_PORTFOLIO", nullable=false)
	private Integer likePortfolio;
	
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPortfolioId(){
		return id;
	}
	
	public User getUser() {
		return user;
	}

	public Set<Photo> getPortfolioPhotos() { return photos; }

	public void setUser(User user) {
		this.user = user;
	}
	
	public void setPortfolioId(Integer id){
		this.id = id;
	}
	
	public String getNomPortfolio(){
		return nomPortfolio;
	}
	
	public void setNomPortfolio(String nomPortfolio){
		this.nomPortfolio = nomPortfolio;
	}
	
	public Integer getLikePortfolio(){
		return likePortfolio;
	}
	
	public void setLikePortfolio(Integer likePortfolio){
		this.likePortfolio = likePortfolio;
	}

}
