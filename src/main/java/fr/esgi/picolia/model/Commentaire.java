package fr.esgi.picolia.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "COMMENTAIRE")
public class Commentaire implements Serializable {
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "USER_ID")
	private User user = new User();

	@ManyToOne
	@JoinColumn(name = "PHOTO_ID")
	private Photo photo = new Photo();
	
	@NotEmpty
	@Column (name="CONTENU", nullable=false)
	private String contenu;
	
	@Column (name="DATE_COM", nullable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateCom;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public Photo getPhoto() {
		return photo;
	}

	public void setPhoto(Photo photo) {
		this.photo = photo;
	}
	
	public String getContenu(){
		return contenu;
	}
	
	public void setContenu(String contenu){
		this.contenu = contenu;
	}
	
	public Date getDateCom(){
		return dateCom;
	}
	
	public void setDateCom(Date dateCom){
		this.dateCom = dateCom;
	}
}
