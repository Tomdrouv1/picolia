package fr.esgi.picolia.model;

import java.io.Serializable;

public enum UserProfileType implements Serializable{
	USER("USER"),
	PHOTOGRAPH("PHOTOGRAPH"),
	ADMIN("ADMIN");
	
	String userProfileType;
	
	private UserProfileType(String userProfileType){
		this.userProfileType = userProfileType;
	}
	
	public String getUserProfileType(){
		return userProfileType;
	}
	
}
