package fr.esgi.picolia.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "PHOTO")
public class Photo implements Serializable {
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "PORTFOLIO_ID")
	private Portfolio portfolio = new Portfolio();

	@OneToMany(cascade=CascadeType.REMOVE, mappedBy = "id")
	private Set<Commentaire> commentaires = new HashSet<>();
	
	@NotEmpty
	@Column(name="NOM_IMAGE", nullable=false)
	private String nomImage;
	
	@Column(name="SRC", nullable=false)
	private String src;
	
	@Column(name="DATE_PHOTO", nullable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date datePhoto;

	@Column(name="LIKE_PHOTO")
	private Integer likePhoto;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public Portfolio getPortfolio() {
		return portfolio;
	}

	public void setPortfolio(Portfolio portfolio) {
		this.portfolio = portfolio;
	}

	public Set<Commentaire> getPhotoCommentaires() { return commentaires; }

	public String getNomImage(){
		return nomImage;
	}
	
	public void setNomImage(String nomImage){
		this.nomImage = nomImage;
	}
	
	public String getSrc(){
		return src;
	}
	
	public void setSrc(String src){
		this.src = src;
	}
	
	public Date getDatePhoto(){
		return datePhoto;
	}
	
	public void setDatePhoto(Date datePhoto){
		this.datePhoto = datePhoto;
	}
	
	public Integer getLikePhoto(){
		return likePhoto;
	}
	
	public void setLikePhoto(Integer likePhoto){
		this.likePhoto = likePhoto;
	}

}
