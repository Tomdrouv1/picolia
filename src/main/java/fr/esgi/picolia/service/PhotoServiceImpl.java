package fr.esgi.picolia.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.esgi.picolia.dao.PhotoDao;
import fr.esgi.picolia.model.Photo;
import fr.esgi.picolia.model.Portfolio;

@Service("photoService")
@Transactional
public class PhotoServiceImpl implements PhotoService{
	
	@Autowired
	private PhotoDao dao;
	
	public Photo findById(int id) {
		return dao.findById(id);
	}
	
	public void savePhoto(Photo photo) {
		dao.save(photo);
	}
	
	public void updatePhoto(Photo photo){
		Photo entity = dao.findById(photo.getId());
		if(entity!=null){
			entity.setNomImage(photo.getNomImage());
			entity.setPortfolio(photo.getPortfolio());
			entity.setSrc(photo.getSrc());
			entity.setDatePhoto(photo.getDatePhoto());
			entity.setLikePhoto(photo.getLikePhoto());
		}
	}
	
	public void deletePhotoByNomImage(String nomImage){
		dao.deleteByNomImage(nomImage);
	}
	
	public List<Photo> findAllPhotos(){
		return dao.findAllPhotos();
	}
	
	public List<Photo> findPhotosByPortfolio(Portfolio portfolio){
		return dao.findPhotosByPortfolio(portfolio);
	}
	
	public void deleteById(Integer id) {
		dao.deleteById(id);
	}
}