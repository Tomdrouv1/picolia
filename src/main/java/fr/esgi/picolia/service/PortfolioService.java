package fr.esgi.picolia.service;

import java.util.List;

import fr.esgi.picolia.model.Portfolio;
import fr.esgi.picolia.model.User;

public interface PortfolioService {
	
	Portfolio findById(Integer id);
	
	void savePortfolio(Portfolio portfolio);
	
	void updatePortfolio(Portfolio portfolio);
	
	void deletePortfolioByNomPortfolio(String portfolio);

	List<Portfolio> findAllPortfolios();

	List<Portfolio> findPortfoliosByUser(User user);

	void deleteById(Integer idportfolio);

	List<Portfolio> findPortfolios(int i); 

}
