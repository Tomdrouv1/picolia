package fr.esgi.picolia.service;

import java.util.List;

import fr.esgi.picolia.model.Photo;
import fr.esgi.picolia.model.Portfolio;

public interface PhotoService {
	
	Photo findById(int id);
	
	void savePhoto(Photo photo);
	
	void updatePhoto(Photo photo);
	
	void deletePhotoByNomImage(String photo);
	
	void deleteById(Integer id);

	List<Photo> findAllPhotos();
	
	List<Photo> findPhotosByPortfolio(Portfolio portfolio); 
}
