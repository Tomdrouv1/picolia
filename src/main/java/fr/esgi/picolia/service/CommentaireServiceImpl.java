package fr.esgi.picolia.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.esgi.picolia.dao.CommentaireDao;
import fr.esgi.picolia.model.Commentaire;
import fr.esgi.picolia.model.Photo;

@Service("commentaireService")
@Transactional
public class CommentaireServiceImpl implements CommentaireService {
	
	@Autowired
	private CommentaireDao dao;
	
	public Commentaire findById(int id) {
		return dao.findById(id);
	}
	
	public Commentaire findByUserId(int id){
		return dao.findByUserId(id);
	}
	
	public void saveCommentaire(Commentaire commentaire) {
		dao.save(commentaire);
	}
	
	public void updateCommentaire(Commentaire commentaire){
		Commentaire entity = dao.findById(commentaire.getId());
		if(entity!=null){
			entity.setUser(commentaire.getUser());
			entity.setPhoto(commentaire.getPhoto());
			entity.setContenu(commentaire.getContenu());
			entity.setDateCom(commentaire.getDateCom());
		}
	}
	
	public void deleteCommentaireByNomImage(int userId){
		dao.deleteByUserId(userId);
	}
	
	public List<Commentaire> findAllCommentaires(){
		return dao.findAllCommentaires();
	}
	
	public List<Commentaire> findCommentairesByPhoto(Photo photo){
		return dao.findCommentairesByPhoto(photo);
	}

	@Override
	public void deleteCommentaireById(String commentaire) {
		// TODO Auto-generated method stub
		
	}

}
