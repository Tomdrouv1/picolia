package fr.esgi.picolia.service;

import java.util.List;

import fr.esgi.picolia.model.UserProfile;


public interface UserProfileService {

	UserProfile findById(int id);

	UserProfile findByType(String type);
	
	List<UserProfile> findAll();
	
}
