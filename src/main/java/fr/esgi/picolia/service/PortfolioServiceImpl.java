package fr.esgi.picolia.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.esgi.picolia.dao.PortfolioDao;
import fr.esgi.picolia.model.Portfolio;
import fr.esgi.picolia.model.User;


@Service("portfolioService")
@Transactional
public class PortfolioServiceImpl implements PortfolioService{

	@Autowired
	private PortfolioDao dao;
	
	public Portfolio findById(Integer id) {
		return dao.findById(id);
	}

	public Portfolio findByNomPortfolio(String nomPortfolio) {
		Portfolio portfolio = dao.findByNomPortfolio(nomPortfolio);
		return portfolio;
	}

	public void savePortfolio(Portfolio portfolio) {
		dao.save(portfolio);
	}

	/*
	 * Since the method is running with Transaction, No need to call hibernate update explicitly.
	 * Just fetch the entity from db and update it with proper values within transaction.
	 * It will be updated in db once transaction ends. 
	 */
	public void updatePortfolio(Portfolio portfolio) {
		Portfolio entity = dao.findById(portfolio.getPortfolioId());
		if(entity!=null){
			entity.setNomPortfolio(portfolio.getNomPortfolio());
			entity.setUser(portfolio.getUser());
			entity.setLikePortfolio(portfolio.getLikePortfolio());
		}
	}

	
	public void deletePortfolioByNomPortfolio(String nomPortfolio) {
		dao.deleteByNomPortfolio(nomPortfolio);
	}
	
	public void deleteById(Integer id) {
		dao.deleteById(id);
	}
	
	public List<Portfolio> findPortfoliosByUser(User user) {
		return dao.findPortfoliosByUser(user);
	}
	

	public List<Portfolio> findAllPortfolios() {
		return dao.findAllPortfolios();
	}
	
	public List<Portfolio> findPortfolios(int i) {
		return dao.findPortfolios(i);
	}
	
}
