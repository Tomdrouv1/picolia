package fr.esgi.picolia.service;

import java.util.List;

import fr.esgi.picolia.model.Commentaire;
import fr.esgi.picolia.model.Photo;

public interface CommentaireService {
	
	Commentaire findById(int id);
	
	void saveCommentaire(Commentaire commentaire);
	
	void updateCommentaire(Commentaire commentaire);
	
	void deleteCommentaireById(String commentaire);

	List<Commentaire> findAllCommentaires(); 
	
	List<Commentaire> findCommentairesByPhoto(Photo photo); 
}
