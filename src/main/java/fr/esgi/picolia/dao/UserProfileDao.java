package fr.esgi.picolia.dao;

import java.util.List;

import fr.esgi.picolia.model.UserProfile;


public interface UserProfileDao {

	List<UserProfile> findAll();
	
	UserProfile findByType(String type);
	
	UserProfile findById(int id);
}
