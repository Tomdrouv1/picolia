package fr.esgi.picolia.dao;

import java.util.List;

import fr.esgi.picolia.model.Photo;
import fr.esgi.picolia.model.Portfolio;


public interface PhotoDao {

	Photo findById(int id);
	
	Photo findByNomImage(String nomImage);
	
	void save(Photo photo);
	
	void deleteByNomImage(String nomImage);
	
	void deleteById(Integer id);
	
	List<Photo> findAllPhotos();
	
	List<Photo> findPhotosByPortfolio(Portfolio portfolio);

}

