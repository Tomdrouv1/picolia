package fr.esgi.picolia.dao;

import java.util.List;

import fr.esgi.picolia.model.Portfolio;
import fr.esgi.picolia.model.User;


public interface PortfolioDao {

	Portfolio findById(Integer id);
	
	Portfolio findByNomPortfolio(String nomPortfolio);
	
	void save(Portfolio portfolio);
	
	void deleteByNomPortfolio(String nomPortfolio);
	
	void deleteById(Integer id);
	
	List<Portfolio> findAllPortfolios();
	
	List<Portfolio> findPortfolios(int number);
	
	List<Portfolio> findPortfoliosByUser(User user);

}

