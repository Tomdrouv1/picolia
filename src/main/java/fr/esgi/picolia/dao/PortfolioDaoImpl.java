package fr.esgi.picolia.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import fr.esgi.picolia.model.Portfolio;
import fr.esgi.picolia.model.User;


@Repository("portfolioDao")
public class PortfolioDaoImpl extends AbstractDao<Integer, Portfolio> implements PortfolioDao {

	static final Logger logger = LoggerFactory.getLogger(PortfolioDaoImpl.class);
	
	public Portfolio findById(Integer id) {
		Portfolio portfolio = getByKey(id);
		if(portfolio!=null){
			Hibernate.initialize(portfolio.getPortfolioId());
		}
		return portfolio;
	}
	
	public Portfolio findByNomPortfolio(String nomPortfolio) {
		logger.info("Nom Album : {}", nomPortfolio);
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("nomPortfolio", nomPortfolio));
		Portfolio portfolio = (Portfolio)crit.uniqueResult();
		if(portfolio!=null){
			Hibernate.initialize(portfolio.getPortfolioId());
		}
		return portfolio;
	}

	@SuppressWarnings("unchecked")
	public List<Portfolio> findAllPortfolios() {
		Criteria criteria = createEntityCriteria().addOrder(Order.asc("nomPortfolio"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);//To avoid duplicates.
		List<Portfolio> portfolios = (List<Portfolio>) criteria.list();
		return portfolios;
	}
	
	@SuppressWarnings("unchecked")
	public List<Portfolio> findPortfolios(int number) {
		Criteria criteria = createEntityCriteria().setMaxResults(number).addOrder(Order.desc("id"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);//To avoid duplicates.
		List<Portfolio> portfolios = (List<Portfolio>) criteria.list();
		return portfolios;
	}
	
	@SuppressWarnings("unchecked")
	public List<Portfolio> findPortfoliosByUser(User user) {
		Criteria criteria = createEntityCriteria().add(Restrictions.eq("user", user)).addOrder(Order.asc("nomPortfolio"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);//To avoid duplicates.
		List<Portfolio> portfolios = (List<Portfolio>) criteria.list();
		return portfolios;
	}

	public void save(Portfolio portfolio) {
		persist(portfolio);
	}

	public void deleteByNomPortfolio(String nomPortfolio) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("nomPortfolio", nomPortfolio));
		Portfolio portfolio = (Portfolio)crit.uniqueResult();
		delete(portfolio);
	}
	
	public void deleteById(Integer id){
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("id", id));
		Portfolio portfolio = (Portfolio)crit.uniqueResult();
		delete(portfolio);
	}

}
