package fr.esgi.picolia.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import fr.esgi.picolia.model.Photo;
import fr.esgi.picolia.model.Portfolio;



@Repository("photoDao")
public class PhotoDaoImpl extends AbstractDao<Integer, Photo> implements PhotoDao {

	static final Logger logger = LoggerFactory.getLogger(PhotoDaoImpl.class);
	
	public Photo findById(int id) {
		Photo photo = getByKey(id);
		if(photo!=null){
			Hibernate.initialize(photo.getId());
		}
		return photo;
	}
	
	public Photo findByNomImage(String nomImage) {
		logger.info("Nom Image : {}", nomImage);
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("nomImage", nomImage));
		Photo photo = (Photo)crit.uniqueResult();
		if(photo!=null){
			Hibernate.initialize(photo.getId());
		}
		return photo;
	}

	@SuppressWarnings("unchecked")
	public List<Photo> findAllPhotos() {
		Criteria criteria = createEntityCriteria().addOrder(Order.asc("nomImage"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);//To avoid duplicates.
		List<Photo> photos = (List<Photo>) criteria.list();
		return photos;
	}
	
	@SuppressWarnings("unchecked")
	public List<Photo> findPhotosByPortfolio(Portfolio portfolio) {
		Criteria criteria = createEntityCriteria().add(Restrictions.eq("portfolio", portfolio)).addOrder(Order.desc("datePhoto"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);//To avoid duplicates.
		List<Photo> photos = (List<Photo>) criteria.list();
		return photos;
	}

	public void save(Photo photo) {
		persist(photo);
	}

	public void deleteByNomImage(String nomImage) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("nomImage", nomImage));
		Photo photo = (Photo)crit.uniqueResult();
		delete(photo);
	}
	
	public void deleteById(Integer id){
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("id", id));
		Photo photo = (Photo)crit.uniqueResult();
		delete(photo);
	}

}
