package fr.esgi.picolia.dao;

import java.util.List;

import fr.esgi.picolia.model.Commentaire;
import fr.esgi.picolia.model.Photo;


public interface CommentaireDao {

	Commentaire findById(int id);
	
	Commentaire findByUserId(int id);
	
	void save(Commentaire commentaire);
	
	void deleteByUserId(int userId);
	
	void deleteById(int id);
	
	List<Commentaire> findAllCommentaires();
	
	List<Commentaire> findCommentairesByPhoto(Photo photo); 

}

