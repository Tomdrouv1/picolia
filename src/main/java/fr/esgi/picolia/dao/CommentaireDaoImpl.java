package fr.esgi.picolia.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import fr.esgi.picolia.model.Commentaire;
import fr.esgi.picolia.model.Photo;



@Repository("commentaireDao")
public class CommentaireDaoImpl extends AbstractDao<Integer, Commentaire> implements CommentaireDao {

	static final Logger logger = LoggerFactory.getLogger(CommentaireDaoImpl.class);
	
	public Commentaire findById(int id) {
		Commentaire commentaire = getByKey(id);
		if(commentaire!=null){
			Hibernate.initialize(commentaire.getId());
		}
		return commentaire;
	}
	
	public Commentaire findByUserId(int id) {
		logger.info("Id : {}", id);
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("id", id));
		Commentaire commentaire = (Commentaire)crit.uniqueResult();
		if(commentaire!=null){
			Hibernate.initialize(commentaire.getId());
		}
		return commentaire;
	}

	@SuppressWarnings("unchecked")
	public List<Commentaire> findAllCommentaires() {
		Criteria criteria = createEntityCriteria().addOrder(Order.asc("photo"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);//To avoid duplicates.
		List<Commentaire> commentaires = (List<Commentaire>) criteria.list();
		return commentaires;
	}
	
	@SuppressWarnings("unchecked")
	public List<Commentaire> findCommentairesByPhoto(Photo photo) {
		Criteria criteria = createEntityCriteria().add(Restrictions.eq("photo", photo)).addOrder(Order.asc("id"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);//To avoid duplicates.
		List<Commentaire> commentaires = (List<Commentaire>) criteria.list();
		return commentaires;
	}

	public void save(Commentaire commentaire) {
		persist(commentaire);
	}
	
	public void deleteById(int id){
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("id", id));
		Commentaire commentaire = (Commentaire)crit.uniqueResult();
		delete(commentaire);
	}
	
	public void deleteByUserId(int userId){
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("user", userId));
		Commentaire commentaire = (Commentaire)crit.uniqueResult();
		delete(commentaire);
	}

}
