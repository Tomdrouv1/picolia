package fr.esgi.picolia.dao;

import java.util.List;

import fr.esgi.picolia.model.User;


public interface UserDao {

	User findById(int id);
	
	User findByUsername(String username);
	
	void save(User user);
	
	void deleteByUsername(String username);
	
	List<User> findAllUsers();

}

