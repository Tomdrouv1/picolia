package fr.esgi.picolia.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.esgi.picolia.model.Portfolio;
import fr.esgi.picolia.model.User;
import fr.esgi.picolia.service.PortfolioService;
import fr.esgi.picolia.service.UserService;



@Controller
@RequestMapping("/")
public class FrontController extends AbstractController{
	
	@Autowired
	PortfolioService portfolioService;
	
	@Autowired
	UserService userService;
	
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String root(ModelMap model) {
    	List<Portfolio> portfolios = portfolioService.findPortfolios(10);
    	User user = userService.findByUsername(getPrincipal());
		model.addAttribute("portfolios", portfolios);
		model.addAttribute("user", user);
		return "index";
    }
}
