package fr.esgi.picolia.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.esgi.picolia.model.Commentaire;
import fr.esgi.picolia.model.Photo;
import fr.esgi.picolia.model.User;
import fr.esgi.picolia.service.CommentaireService;
import fr.esgi.picolia.service.PhotoService;
import fr.esgi.picolia.service.UserService;

@Controller
@RequestMapping("/photo")
public class PhotoController extends AbstractController {
	
	@Autowired
	PhotoService photoService;
	
	@Autowired
	CommentaireService commentaireService;
	
	@Autowired
	UserService userService;
	
	/**
	 * Photo page.
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String photoPage(@PathVariable int id, ModelMap model) {
		Photo photo = photoService.findById(id);
		Commentaire commentaire = new Commentaire();
		User user = userService.findByUsername(getPrincipal());
		List<Commentaire> commentaires = commentaireService.findCommentairesByPhoto(photo);
		model.addAttribute("photo", photo);
		model.addAttribute("commentaires", commentaires);
		model.addAttribute("commentaire", commentaire);
		model.addAttribute("connected", getPrincipal());
		model.addAttribute("user", user);
		if (photo == null){
			return "404";
		}
		return "photo";
	}
	
	@RequestMapping(value = { "/{id}" }, method = RequestMethod.POST)
	public String savePortfolio(@Valid Commentaire commentaire, BindingResult result, @PathVariable int id, ModelMap model) {

		if (result.hasErrors()) {
			return "redirect:/photo/{id}";
		}
		User user = userService.findByUsername(getPrincipal());	
		commentaire.setUser(user);

		Photo photo = photoService.findById(id);
		commentaire.setPhoto(photo);

		commentaireService.saveCommentaire(commentaire);

		return "redirect:/photo/{id}/";
	}
}
