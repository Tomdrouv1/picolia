package fr.esgi.picolia.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.esgi.picolia.model.Commentaire;
import fr.esgi.picolia.model.Photo;
import fr.esgi.picolia.model.Portfolio;
import fr.esgi.picolia.model.User;
import fr.esgi.picolia.service.CommentaireService;
import fr.esgi.picolia.service.PhotoService;
import fr.esgi.picolia.service.PortfolioService;
import fr.esgi.picolia.service.UserService;



@Controller
@RequestMapping("/admin")
public class AdminController extends AbstractController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	PortfolioService portfolioService;
	
	@Autowired
	PhotoService photoService;
	
	@Autowired
	CommentaireService commentaireService;
	
	@Autowired
	MessageSource messageSource;
	
	@Autowired
	AuthenticationTrustResolver authenticationTrustResolver;

	@Autowired
	ServletContext context;
	
	/**
	 * This method will list all existing users.
	 */
	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public String adminIndex(ModelMap model) {
		return "adminindex";
	}
	
	/**
	 * This method will list all existing users.
	 */
	@RequestMapping(value = { "/users" }, method = RequestMethod.GET)
	public String listUsers(ModelMap model) {

		List<User> users = userService.findAllUsers();
		model.addAttribute("users", users);
		model.addAttribute("loggedinuser", getPrincipal());
		return "adminuserslist";
	}
	
	/**
	 * This method will provide the medium to add a new user.
	 */
	@RequestMapping(value = { "/users/new" }, method = RequestMethod.GET)
	public String newUser(ModelMap model) {
		User user = new User();
		model.addAttribute("user", user);
		model.addAttribute("edit", false);
		model.addAttribute("loggedinuser", getPrincipal());
		return "registration";
	}
	
	/**
	 * This method will be called on form submission, handling POST request for
	 * saving user in database. It also validates the user input
	 */
	@RequestMapping(value = { "/users/new" }, method = RequestMethod.POST)
	public String saveUser(@Valid User user, BindingResult result,
						   ModelMap model) {

		if (result.hasErrors()) {
			return "registration";
		}

		if(!userService.isUsernameUnique(user.getId(), user.getUsername())){
			FieldError usernameError =new FieldError("user","username", messageSource.getMessage("non.unique.username", new String[]{user.getUsername()}, Locale.getDefault()));
		    result.addError(usernameError);
			return "registration";
		}
		
		userService.saveUser(user);

		model.addAttribute("success", "User " + user.getFirstName() + " "+ user.getLastName() + " registered successfully");
		model.addAttribute("loggedinuser", getPrincipal());
		
		return "registrationsuccess";

	}
	
	/**
	 * This method will provide the medium to update an existing user.
	 */
	@RequestMapping(value = { "/users/edit/{username}" }, method = RequestMethod.GET)
	public String editUser(@PathVariable String username, ModelMap model) {
		User user = userService.findByUsername(username);
		model.addAttribute("user", user);
		model.addAttribute("edit", true);
		model.addAttribute("loggedinuser", getPrincipal());
		return "registration";
	}
	
	/**
	 * This method will be called on form submission, handling POST request for
	 * updating user in database. It also validates the user input
	 */
	@RequestMapping(value = { "/users/edit/{username}" }, method = RequestMethod.POST)
	public String updateUser(@Valid User user, BindingResult result, ModelMap model) {

		if (result.hasErrors()) {
			return "registration";
		}

		userService.updateUser(user);

		model.addAttribute("success", "User " + user.getFirstName() + " "+ user.getLastName() + " updated successfully");
		model.addAttribute("loggedinuser", getPrincipal());
		return "registrationsuccess";
	}

	
	/**
	 * This method will delete an user by it's username.
	 */
	@RequestMapping(value = { "/users/delete/{username}" }, method = RequestMethod.GET)
	public String deleteUser(@PathVariable String username) {
		userService.deleteUserByUsername(username);
		return "redirect:/admin/users/";
	}
	
	
	/**
	 * This method will list all existing users.
	 */
	@RequestMapping(value = { "/portfolios" }, method = RequestMethod.GET)
	public String listPortfolios(ModelMap model) {

		List<Portfolio> portfolios = portfolioService.findAllPortfolios();
		model.addAttribute("portfolios", portfolios);
		model.addAttribute("loggedinuser", getPrincipal());
		return "adminportfolioslist";
	}
	
	/**
	 * This method will provide the medium to add a new user.
	 */
	@RequestMapping(value = { "/portfolios/new" }, method = RequestMethod.GET)
	public String newAdminPorfolio(ModelMap model) {
		Portfolio portfolio = new Portfolio();
		model.addAttribute("portfolio", portfolio);
		List<User> users = userService.findAllUsers();
		model.addAttribute("users", users);
		model.addAttribute("edit", false);
		model.addAttribute("loggedinuser", getPrincipal());
		return "adminnewportfolio";
	}
	
	/**
	 * This method will be called on form submission, handling POST request for
	 * saving user in database. It also validates the user input
	 */
	@RequestMapping(value = { "/portfolios/new" }, method = RequestMethod.POST)
	public String saveAdminPortfolio(@RequestParam("userId") Integer userId, @Valid Portfolio portfolio, BindingResult result,
						   ModelMap model) {

		if (result.hasErrors()) {
			return "adminnewportfolio";
		}
		
		User user = userService.findById(userId);
		portfolio.setUser(user);
		portfolioService.savePortfolio(portfolio);
		
		return "redirect:/admin/portfolios";
	}
	
	/**
	 * This method will provide the medium to update an existing user.
	 */
	@RequestMapping(value = { "/portfolios/edit/{id}" }, method = RequestMethod.GET)
	public String editAdminPortfolio(@PathVariable int id, ModelMap model) {
		Portfolio portfolio = portfolioService.findById(id);
		model.addAttribute("portfolio", portfolio);
		List<User> users = userService.findAllUsers();
		model.addAttribute("users", users);
		model.addAttribute("edit", true);
		model.addAttribute("loggedinuser", getPrincipal());
		return "adminnewportfolio";
	}
	
	/**
	 * This method will be called on form submission, handling POST request for
	 * updating user in database. It also validates the user input
	 */
	@RequestMapping(value = { "/portfolios/edit/{id}" }, method = RequestMethod.POST)
	public String updateAdminPortfolio(@RequestParam("userId") Integer userId, @Valid Portfolio portfolio, BindingResult result, ModelMap model) {

		if (result.hasErrors()) {
			return "adminportfolioslist";
		}
		User user = userService.findById(userId);
		portfolio.setUser(user);
		
		portfolioService.updatePortfolio(portfolio);
		return "redirect:/admin/portfolios/";
	}
	
	/**
	 * This method will delete an user by it's username.
	 */
	@RequestMapping(value = { "/portfolios/delete/{id}" }, method = RequestMethod.GET)
	public String deleteAdminPortfolio(@PathVariable int id) {
		portfolioService.deleteById(id);
		return "redirect:/admin/portfolios/";
	}
	
	/**
	 * This method will list all existing users.
	 */
	@RequestMapping(value = { "/photos" }, method = RequestMethod.GET)
	public String listAdminPhotos(ModelMap model) {

		List<Photo> photos = photoService.findAllPhotos();
		model.addAttribute("photos", photos);
		model.addAttribute("loggedinuser", getPrincipal());
		return "adminphotoslist";
	}
	
	/**
	 * This method will provide the medium to add a new user.
	 */
	@RequestMapping(value = { "/photos/new" }, method = RequestMethod.GET)
	public String newAdminPhoto(ModelMap model) {
		Photo photo = new Photo();
		model.addAttribute("photo", photo);
		List<Portfolio> portfolios = portfolioService.findAllPortfolios();
		model.addAttribute("portfolios", portfolios);
		model.addAttribute("edit", false);
		model.addAttribute("loggedinuser", getPrincipal());
		return "adminnewphoto";
	}
	
	/**
	 * This method will be called on form submission, handling POST request for
	 * saving user in database. It also validates the user input
	 */
	@RequestMapping(value = { "/photos/new" }, method = RequestMethod.POST)
	public String saveAdminPhoto(@RequestParam("file") MultipartFile file, @RequestParam("pId") Integer pId, RedirectAttributes redirectAttributes, @Valid Photo photo, BindingResult result,
						   ModelMap model) {

		if (result.hasErrors()) {
			return "adminnewphoto";
		}
		Portfolio p = portfolioService.findById(pId);
		photo.setPortfolio(p);
		
		if (!file.isEmpty()) {
			try {
				BufferedImage src = ImageIO.read(new ByteArrayInputStream(file.getBytes()));
				String parent = context.getResource("static/uploads/").getFile();

				File destination = new File(parent.concat(file.getOriginalFilename()));
				if(file.getOriginalFilename().contains(".jpg")) {
					ImageIO.write(src, "jpg", destination);
				} else if(file.getOriginalFilename().contains(".png")) {
					ImageIO.write(src, "png", destination);
				}

				redirectAttributes.addFlashAttribute("message",
						"You successfully uploaded " + file.getOriginalFilename() + "!");
			} catch (IOException|RuntimeException e) {
				redirectAttributes.addFlashAttribute("message", "Failed to upload " + file.getOriginalFilename() + " => " + e.getMessage());
			}
			photo.setSrc(file.getOriginalFilename());
		} else {
			redirectAttributes.addFlashAttribute("message", "Failed to upload " + file.getOriginalFilename() + " because it was empty");
		}
		photoService.savePhoto(photo);
		
		return "redirect:/admin/photos";
	}
	
	/**
	 * This method will provide the medium to update an existing user.
	 */
	@RequestMapping(value = { "/photos/edit/{id}" }, method = RequestMethod.GET)
	public String editAdminPhoto(@PathVariable int id, ModelMap model) {
		Photo photo = photoService.findById(id);		
		model.addAttribute("photo", photo);
		List<Portfolio> portfolios = portfolioService.findAllPortfolios();
		model.addAttribute("portfolios", portfolios);
		model.addAttribute("edit", true);
		model.addAttribute("loggedinuser", getPrincipal());
		return "adminnewphoto";
	}
	
	/**
	 * This method will be called on form submission, handling POST request for
	 * updating user in database. It also validates the user input
	 */
	@RequestMapping(value = { "/photos/edit/{id}" }, method = RequestMethod.POST)
	public String updateAdminPhoto(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes, @RequestParam("pId") Integer portfolioId, @Valid Photo photo, BindingResult result, ModelMap model) {

		if (result.hasErrors()) {
			return "adminnewphoto";
		}
		
		if (!file.isEmpty()) {
			try {
				BufferedImage src = ImageIO.read(new ByteArrayInputStream(file.getBytes()));
				String parent = context.getResource("static/uploads/").getFile();

				File destination = new File(parent.concat(file.getOriginalFilename()));
				if(file.getOriginalFilename().contains(".jpg")) {
					ImageIO.write(src, "jpg", destination);
				} else if(file.getOriginalFilename().contains(".png")) {
					ImageIO.write(src, "png", destination);
				}

				redirectAttributes.addFlashAttribute("message",
						"You successfully uploaded " + file.getOriginalFilename() + "!");
			} catch (IOException|RuntimeException e) {
				redirectAttributes.addFlashAttribute("message", "Failed to upload " + file.getOriginalFilename() + " => " + e.getMessage());
			}
			photo.setSrc(file.getOriginalFilename());
		} else {
			redirectAttributes.addFlashAttribute("message", "Failed to upload " + file.getOriginalFilename() + " because it was empty");
		}
		
		Portfolio portfolio = portfolioService.findById(portfolioId);
		photo.setPortfolio(portfolio);

		photoService.updatePhoto(photo);
		return "redirect:/admin/photos/";
	}
	
	/**
	 * This method will delete an user by it's username.
	 */
	@RequestMapping(value = { "/photos/delete/{id}" }, method = RequestMethod.GET)
	public String deleteAdminPhoto(@PathVariable int id) {
		photoService.deleteById(id);
		return "redirect:/admin/photos/";
	}
	
	/**
	 * This method will list all existing users.
	 */
	@RequestMapping(value = { "/commentaires" }, method = RequestMethod.GET)
	public String listAdminCommentaires(ModelMap model) {

		List<Commentaire> commentaires = commentaireService.findAllCommentaires();
		model.addAttribute("commentaires", commentaires);
		model.addAttribute("loggedinuser", getPrincipal());
		return "admincommentaireslist";
	}
	
	/**
	 * This method will provide the medium to add a new user.
	 */
	@RequestMapping(value = { "/commentaires/new" }, method = RequestMethod.GET)
	public String newAdminCommentaire(ModelMap model) {
		Commentaire commentaire = new Commentaire();
		model.addAttribute("commentaire", commentaire);
		List<User> users = userService.findAllUsers();
		List<Photo> photos = photoService.findAllPhotos();
		model.addAttribute("users", users);
		model.addAttribute("photos", photos);
		model.addAttribute("edit", false);
		model.addAttribute("loggedinuser", getPrincipal());
		return "adminnewcommentaire";
	}
	
	/**
	 * This method will be called on form submission, handling POST request for
	 * saving user in database. It also validates the user input
	 */
	@RequestMapping(value = { "/commentaires/new" }, method = RequestMethod.POST)
	public String saveAdminCommentaire(@RequestParam("userId") Integer userId, @RequestParam("photoId") Integer photoId, @Valid Commentaire commentaire, BindingResult result,
						   ModelMap model) {

		if (result.hasErrors()) {
			return "adminnewcommentaire";
		}
		
		User user = userService.findById(userId);
		commentaire.setUser(user);
		Photo photo = photoService.findById(photoId);
		commentaire.setPhoto(photo);
		commentaireService.saveCommentaire(commentaire);
		
		return "redirect:/admin/commentaires";
	}
	
	/**
	 * This method will provide the medium to update an existing user.
	 */
	@RequestMapping(value = { "/commentaires/edit/{id}" }, method = RequestMethod.GET)
	public String editAdminCom(@PathVariable int id, ModelMap model) {
		Commentaire com = commentaireService.findById(id);
		model.addAttribute("commentaire", com);
		List<Commentaire> commentaires = commentaireService.findAllCommentaires();
		model.addAttribute("commentaires", commentaires);
		model.addAttribute("edit", true);
		model.addAttribute("loggedinuser", getPrincipal());
		return "adminnewcommentaire";
	}
	
	/**
	 * This method will be called on form submission, handling POST request for
	 * updating user in database. It also validates the user input
	 */
	@RequestMapping(value = { "/commentaires/edit/{id}" }, method = RequestMethod.POST)
	public String updateAdminCom(@RequestParam("pfId") Integer portfolioId, @RequestParam("userId") Integer userId, @Valid Commentaire com, BindingResult result, ModelMap model) {

		if (result.hasErrors()) {
			return "adminnewcommentaire";
		}

		User user = userService.findById(userId);
		com.setUser(user);
		
		Photo photo = photoService.findById(portfolioId);
		com.setPhoto(photo);
		
		commentaireService.updateCommentaire(com);
		return "redirect:/admin/commentaires/";
	}

}
