package fr.esgi.picolia.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.esgi.picolia.model.Portfolio;
import fr.esgi.picolia.model.User;
import fr.esgi.picolia.model.Photo;
import fr.esgi.picolia.service.PortfolioService;
import fr.esgi.picolia.service.UserService;
import fr.esgi.picolia.service.PhotoService;

@Controller
@RequestMapping("/portfolio")
public class PortfolioController extends AbstractController {
	
	@Autowired
	PortfolioService portfolioService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	PhotoService photoService;
	
	@Autowired
	AuthenticationTrustResolver authenticationTrustResolver;

	@Autowired
	ServletContext context;

	public static final String ROOT = System.getProperty("user.dir");
	
	/**
	 * This method will list all existing portfolios.
	 */
	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public String listPortfolio(ModelMap model) {
		if (isCurrentAuthenticationAnonymous()) {
			return "redirect:/login/";
	    } else {
	    	User user = userService.findByUsername(getPrincipal());
			List<Portfolio> portfolios = portfolioService.findPortfoliosByUser(user);
			model.addAttribute("portfolios", portfolios);
			model.addAttribute("loggedinuser", getPrincipal());
			model.addAttribute("user", user);
			return "portfolioslist";
	    }
	}
	
	/**
	 * This method will display all photos in a portfolio.
	 */
	@RequestMapping(value = { "/{idportfolio}" }, method = RequestMethod.GET)
	public String Portfolio(@PathVariable Integer idportfolio, ModelMap model) {
		Portfolio portfolio = portfolioService.findById(idportfolio);
		User user = userService.findByUsername(getPrincipal());
		boolean owner = false;
		if (!isCurrentAuthenticationAnonymous() || portfolio != null) {
			User userPortfolio = portfolio.getUser();
			if(userPortfolio.getId() == user.getId()) {
				owner = true;
			}
		}
		List<Photo> photos = photoService.findPhotosByPortfolio(portfolio);
		Photo photo = new Photo();
		model.addAttribute("photo", photo);
		model.addAttribute("portfolio", portfolio);
		model.addAttribute("photos", photos);
		model.addAttribute("user", user);
		model.addAttribute("owner", owner);
		return "portfolio";
	}
	
	/**
	 * This method will provide the medium to add a new portfolio.
	 */
	@RequestMapping(value = { "/new" }, method = RequestMethod.GET)
	public String newPortfolio(ModelMap model) {
		if (isCurrentAuthenticationAnonymous()) {
			return "redirect:/login/";
	    } else {
			Portfolio portfolio = new Portfolio();
			model.addAttribute("portfolio", portfolio);
			return "newportfolio";
	    }
	}
	
	/**
	 * This method will be called on form submission, handling POST request for
	 * saving portfolio in database. It also validates the portfolio input
	 */
	@RequestMapping(value = { "/new" }, method = RequestMethod.POST)
	public String savePortfolio(@Valid Portfolio portfolio, BindingResult result, ModelMap model) {

		if (result.hasErrors()) {
			return "newportfolio";
		}
		User user = userService.findByUsername(getPrincipal());
		portfolio.setUser(user);
		
		portfolioService.savePortfolio(portfolio);

		return "redirect:/portfolio/";
	}
	
	/**
	 * This method will delete a portfolio by its id.
	 */
	@RequestMapping(value = { "/delete/{idportfolio}" }, method = RequestMethod.GET)
	public String deleteUser(@PathVariable Integer idportfolio) {
		User user = userService.findByUsername(getPrincipal());
		Portfolio portfolio = portfolioService.findById(idportfolio);
		User userPortfolio = portfolio.getUser();
		
		if(userPortfolio.getId() == user.getId()) {
			portfolioService.deleteById(idportfolio);
		}
		return "redirect:/portfolio/";
	}
	
	/**
	 * This method will add a photo in a portfolio.
	 */
	@RequestMapping(value = { "/{idportfolio}/add" }, method = RequestMethod.POST)
	public String addPhoto(@RequestParam("file") MultipartFile file, @PathVariable Integer idportfolio, RedirectAttributes redirectAttributes, @Valid Photo photo, BindingResult result, ModelMap model) {
		if (result.hasErrors()) {
			return "redirect:/portfolio/";
		}
		Portfolio portfolio = portfolioService.findById(idportfolio);
		photo.setPortfolio(portfolio);
		
		if (!file.isEmpty()) {
			try {
				BufferedImage src = ImageIO.read(new ByteArrayInputStream(file.getBytes()));
				String parent = context.getResource("static/uploads/").getFile();

				File destination = new File(parent.concat(file.getOriginalFilename()));
				if(file.getOriginalFilename().contains(".jpg")) {
					ImageIO.write(src, "jpg", destination);
				} else if(file.getOriginalFilename().contains(".png")) {
					ImageIO.write(src, "png", destination);
				}

				redirectAttributes.addFlashAttribute("message",
						"You successfully uploaded " + file.getOriginalFilename() + "!");
			} catch (IOException|RuntimeException e) {
				redirectAttributes.addFlashAttribute("message", "Failed to upload " + file.getOriginalFilename() + " => " + e.getMessage());
			}
			photo.setSrc(file.getOriginalFilename());
		} else {
			redirectAttributes.addFlashAttribute("message", "Failed to upload " + file.getOriginalFilename() + " because it was empty");
		}
		photoService.savePhoto(photo);
		return "redirect:/portfolio/{idportfolio}";
	}
	
	
	/**
	 * This method returns true if users is already authenticated [logged-in], else false.
	 */
	private boolean isCurrentAuthenticationAnonymous() {
	    final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	    return authenticationTrustResolver.isAnonymous(authentication);
	}
}
