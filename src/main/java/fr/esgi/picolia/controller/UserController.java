package fr.esgi.picolia.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import fr.esgi.picolia.model.Portfolio;
import fr.esgi.picolia.model.User;
import fr.esgi.picolia.model.UserProfile;
import fr.esgi.picolia.service.PortfolioService;
import fr.esgi.picolia.service.UserProfileService;
import fr.esgi.picolia.service.UserService;



@Controller
@RequestMapping("/")
@SessionAttributes("roles")
public class UserController extends AbstractController {

	@Autowired
	UserService userService;

	@Autowired
	UserProfileService userProfileService;
	
	@Autowired
	PortfolioService portfolioService;

	@Autowired
	MessageSource messageSource;

	@Autowired
	PersistentTokenBasedRememberMeServices persistentTokenBasedRememberMeServices;

	@Autowired
	AuthenticationTrustResolver authenticationTrustResolver;


	/**
	 * This method will provide UserProfile list to views
	 */
	@ModelAttribute("roles")
	public List<UserProfile> initializeProfiles() {
		return userProfileService.findAll();
	}
	
	/**
	 * This method handles Access-Denied redirect.
	 */
	@RequestMapping(value = "/Access_Denied", method = RequestMethod.GET)
	public String accessDeniedPage(ModelMap model) {
		model.addAttribute("loggedinuser", getPrincipal());
		return "accessDenied";
	}

	/**
	 * This method handles register GET requests.
	 * If users is already logged-in and tries to goto register page again, will be redirected to home page.
     */
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String registerPage(ModelMap model) {
		User user = new User();
		model.addAttribute("user", user);
		model.addAttribute("edit", false);
		if(isCurrentAuthenticationAnonymous()) {
			return "register";
		}
		return "redirect:/";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String registerPost(@Valid User user, BindingResult result, ModelMap model) {
		if(result.hasErrors()) {
			return "register";
		}
		if(!userService.isUsernameUnique(user.getId(), user.getUsername())){
			FieldError usernameError =new FieldError("user","username", messageSource.getMessage("non.unique.username", new String[]{user.getUsername()}, Locale.getDefault()));
			result.addError(usernameError);
			return "register";
		}
		userService.saveUser(user);
		model.addAttribute("loggedinuser", user.getUsername());
		return "registersuccess";
	}

	/**
	 * This method handles login GET requests.
	 * If users is already logged-in and tries to goto login page again, will be redirected to list page.
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginPage() {
		if (isCurrentAuthenticationAnonymous()) {
			return "login";
	    } else {
	    	return "redirect:/users";
	    }
	}

	/**
	 * This method handles logout requests.
	 * Toggle the handlers if you are RememberMe functionality is useless in your app.
	 */
	@RequestMapping(value="/logout", method = RequestMethod.GET)
	public String logoutPage (HttpServletRequest request, HttpServletResponse response){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null){
			persistentTokenBasedRememberMeServices.logout(request, response, auth);
			SecurityContextHolder.getContext().setAuthentication(null);
		}
		return "redirect:/login?logout";
	}
	
	/**
	 * Profile page.
	 */
	@RequestMapping(value = "/user/{username}", method = RequestMethod.GET)
	public String profilePage(@PathVariable String username, ModelMap model) {
		User user = userService.findByUsername(username);
		User currentUser = userService.findByUsername(getPrincipal());
		boolean owner = false;
		if(currentUser.getId() == user.getId()) {
			owner = true;
		}
		model.addAttribute("owner", owner);
		List<Portfolio> portfolios = portfolioService.findPortfoliosByUser(user);
		model.addAttribute("portfolios", portfolios);
		model.addAttribute("user", user);
		model.addAttribute("currentuser", getPrincipal());
		return "profile";
	}
	
	/**
	 * Edit profile page.
	 */
	@RequestMapping(value = "/user/{username}/edit", method = RequestMethod.GET)
	public String profileEditPage(@PathVariable String username, ModelMap model) {
		User user = userService.findByUsername(username);
		User currentUser = userService.findByUsername(getPrincipal());
		boolean owner = false;
		if(currentUser.getId() == user.getId()) {
			owner = true;
		}
		
		model.addAttribute("edit", true);
		model.addAttribute("user", user);
		if (!owner) {
			return "accessDenied";
		}
		return "profileEdit";
	}
	
	/**
	 * This method will be called on form submission, handling POST request for
	 * updating user in database. It also validates the user input
	 */
	@RequestMapping(value = { "/user/{username}/edit" }, method = RequestMethod.POST)
	public String updateProfile(@Valid User user, BindingResult result, ModelMap model) {

		if (result.hasErrors()) {
			return "redirect:/user/{username}/edit";
		}

		userService.updateUser(user);

		return "redirect:/user/{username}/";
	}

	
	/**
	 * This method returns true if users is already authenticated [logged-in], else false.
	 */
	private boolean isCurrentAuthenticationAnonymous() {
	    final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	    return authenticationTrustResolver.isAnonymous(authentication);
	}


}