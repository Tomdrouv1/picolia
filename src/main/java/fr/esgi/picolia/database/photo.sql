CREATE TABLE PHOTO (
  id BIGINT NOT NULL AUTO_INCREMENT,
  portfolio_id BIGINT NOT NULL,
  nom_image varchar(255) NOT NULL,
  src varchar(255) NOT NULL,
  date_photo TIMESTAMP,
  like_photo int,
  PRIMARY KEY (id),
  CONSTRAINT FOREIGN KEY (portfolio_id) REFERENCES PORTFOLIO (id)
);