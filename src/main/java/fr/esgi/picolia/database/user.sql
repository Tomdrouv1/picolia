create table USER (
  id BIGINT NOT NULL AUTO_INCREMENT,
  username VARCHAR(30) NOT NULL,
  password VARCHAR(100) NOT NULL,
  first_name VARCHAR(30),
  last_name  VARCHAR(30),
  description VARCHAR(255),
  facebook_url VARCHAR(50),
  twitter_url VARCHAR(50),
  tumblr_url VARCHAR(50),
  pinterest_url VARCHAR(50),
  linkedin_url VARCHAR(50),
  google_url VARCHAR(50),
  email VARCHAR(30) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE (username)
);

/* USER_PROFILE table contains all possible roles */
create table USER_PROFILE(
  id BIGINT NOT NULL AUTO_INCREMENT,
  type VARCHAR(30) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE (type)
);

/* JOIN TABLE for MANY-TO-MANY relationship*/
CREATE TABLE USER_USER_PROFILE (
  user_id BIGINT NOT NULL,
  user_profile_id BIGINT NOT NULL,
  PRIMARY KEY (user_id, user_profile_id),
  CONSTRAINT FK_USER FOREIGN KEY (user_id) REFERENCES USER (id),
  CONSTRAINT FK_USER_PROFILE FOREIGN KEY (user_profile_id) REFERENCES USER_PROFILE (id)
);

/* Populate USER_PROFILE Table */
INSERT INTO USER_PROFILE(type)
VALUES ('USER');

INSERT INTO USER_PROFILE(type)
VALUES ('ADMIN');

INSERT INTO USER_PROFILE(type)
VALUES ('PHOTOGRAPH');


/* Populate one Admin User which will further create other users for the application using GUI */
INSERT INTO USER(username, password, first_name, last_name, email)
VALUES ('sam','$2a$10$4eqIF5s/ewJwHK1p8lqlFOEm2QIA0S8g6./Lok.pQxqcxaBZYChRm', 'Sam','Smith','samy@xyz.com');


/* Populate JOIN Table */
INSERT INTO USER_USER_PROFILE (user_id, user_profile_id)
  SELECT user.id, profile.id FROM user user, user_profile profile
  where user.username='sam' and profile.type='ADMIN';

/* Create persistent_logins Table used to store rememberme related stuff*/
CREATE TABLE persistent_logins (
  username VARCHAR(64) NOT NULL,
  series VARCHAR(64) NOT NULL,
  token VARCHAR(64) NOT NULL,
  last_used TIMESTAMP NOT NULL,
  PRIMARY KEY (series)
);