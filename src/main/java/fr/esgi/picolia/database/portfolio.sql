CREATE TABLE PORTFOLIO (
  id BIGINT NOT NULL AUTO_INCREMENT,
  user_id BIGINT NOT NULL,
  nom_portfolio varchar(255) NOT NULL,
  like_portfolio int,
  PRIMARY KEY (id),
  CONSTRAINT FOREIGN KEY (user_id) REFERENCES USER (id)
);