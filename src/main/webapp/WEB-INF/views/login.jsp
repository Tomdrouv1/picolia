<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Picolia</title>
		<link href="<c:url value='/static/css/bootstrap.css' />"  rel="stylesheet"></link>
		<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
		<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css" />
	</head>

<body>
		<div class="login-container">
			<div class="login-card">
				<h2>Bienvenue sur <span>Picolia</span></h2>
				<div class="login-form">
					<c:url var="loginUrl" value="/login" />
					<form action="${loginUrl}" method="post" class="form-horizontal">
						<c:if test="${param.error != null}">
							<div class="alert alert-danger">
								<p>Nom d'utilisateur et mot de passe incorrects.</p>
							</div>
						</c:if>
						<c:if test="${param.logout != null}">
							<div class="alert alert-success">
								<p>Vous avez bien �t� d�connect�</p>
							</div>
						</c:if>
						<div class="input-group input-sm">
							<label class="input-group-addon" for="username"><i class="fa fa-user"></i></label>
							<input type="text" class="form-control" id="username" name="username" placeholder="Nom d'utilisateur" required>
						</div>
						<div class="input-group input-sm">
							<label class="input-group-addon" for="password"><i class="fa fa-lock"></i></label> 
							<input type="password" class="form-control" id="password" name="password" placeholder="Mot de passe" required>
						</div>
						<div class="input-group input-sm">
                             <div class="checkbox">
                               <label><input type="checkbox" id="rememberme" name="remember-me"> Se souvenir de moi</label>  
                             </div>
                           </div>
						<input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />
							
						<div class="form-actions">
							<input type="submit"
								class="btn btn-block btn-primary btn-default" value="Connexion">
						</div>
					</form>
					<a href="<c:url value="/register" />" class="btn btn-outline btn-block btn-default">S'inscrire</a>
				</div>
			</div>
		</div>

	</body>
</html>