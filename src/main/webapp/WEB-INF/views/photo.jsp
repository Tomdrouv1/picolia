<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Photo ${photo.nomImage}</title>
	<link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
	<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>

<body>
	<%@include file="authheader.jsp" %>
	<div class="generic-container">
		<div class="generic-content displayPhoto">
			<div class="photo">
				<img src='<c:url value="/static/uploads/${photo.src}"></c:url>' />
				<div class="title">Photo ${photo.nomImage}</div>
			</div>
			<h3>Commentaires</h3>
			<ul class="comms">
				<c:forEach items="${commentaires}" var="commentaire">
					<li>
						<strong class="title">${commentaire.user.firstName} ${commentaire.user.lastName} (${commentaire.user.username}) :</strong>
						${commentaire.contenu}
					</li>
				</c:forEach>
			</ul>
			<h4>Ajouter un commentaire</h4>
			<c:choose>
				<c:when test="${connected != 'anonymousUser'}">
					<form:form method="POST" modelAttribute="commentaire" class="form-horizontal" command="commentaire">
						<form:input type="hidden" path="id" id="id"/>
						<div class="row">
							<div class="form-group col-md-12">
								<label class="col-md-3 control-lable" for="nomPortfolio">Contenu</label>
								<div class="col-md-7">
									<form:input type="text" path="contenu" id="contenu" class="form-control input-sm"/>
									<div class="has-error">
										<form:errors path="contenu" class="help-inline"/>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-actions floatRight">
								<input type="submit" value="Valider" class="btn btn-primary btn-sm"/>
							</div>
						</div>
					</form:form>
				</c:when>
				<c:otherwise>
					Vous devez �tre connect� pour ajouter un commentaire. <br>
					<a href="<c:url value='/login' />">Se connecter</a>
				</c:otherwise>
			</c:choose>		
		</div>
	</div>
</body>