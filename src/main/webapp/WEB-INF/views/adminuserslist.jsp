<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Liste des utilisateurs</title>
	<link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
	<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>

<body>
	<%@include file="authheader.jsp" %>	
	<div class="generic-container">
		<div class="panel panel-default">
			  <!-- Default panel contents -->
		  	<div class="panel-heading"><span class="lead">Liste des utilisateurs</span></div>
			<table class="table table-hover">
	    		<thead>
		      		<tr>
				        <th>Pr�nom</th>
				        <th>Nom</th>
				        <th>Adresse mail</th>
				        <th>Nom d'utilisateur</th>
				        <sec:authorize access="hasRole('ADMIN') or hasRole('PHOTOGRAPH')">
				        	<th width="100"></th>
				        </sec:authorize>
				        <sec:authorize access="hasRole('ADMIN')">
				        	<th width="100"></th>
				        </sec:authorize>
				        
					</tr>
		    	</thead>
	    		<tbody>
				<c:forEach items="${users}" var="user">
					<tr>
						<td>${user.firstName}</td>
						<td>${user.lastName}</td>
						<td>${user.email}</td>
						<td>${user.username}</td>
					    <sec:authorize access="hasRole('ADMIN') or hasRole('PHOTOGRAPHE')">
							<td><a href="<c:url value='/admin/users/edit/${user.username}' />" class="btn btn-success custom-width">Editer</a></td>
				        </sec:authorize>
				        <sec:authorize access="hasRole('ADMIN')">
							<td><a href="<c:url value='/admin/users/delete/${user.username}' />" class="btn btn-danger custom-width">Supprimer</a></td>
        				</sec:authorize>
					</tr>
				</c:forEach>
	    		</tbody>
	    	</table>
		</div>
		<sec:authorize access="hasRole('ADMIN')">
		 	<div class="well">
		 		<a href="<c:url value='/admin/users/new' />">Ajouter un utilisateur</a>
		 	</div>
	 	</sec:authorize>
   	</div>
</body>
</html>