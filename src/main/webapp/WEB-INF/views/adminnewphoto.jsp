<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Formulaire d'inscription</title>
	<link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
	<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>

<body>
	<%@include file="authheader.jsp" %>
 	<div class="generic-container">

		<div class="well lead">Formulaire photo</div>
	 	<form:form action="?${_csrf.parameterName}=${_csrf.token}" method="POST" modelAttribute="photo" class="form-horizontal" command="photo" enctype="multipart/form-data">
			<form:input type="hidden" path="id" id="id"/>
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-3 control-lable" for="nomImage">Titre photo</label>
					<div class="col-md-7">
						<form:input type="text" path="nomImage" id="nomImage" class="form-control input-sm"/>
						<div class="has-error">
							<form:errors path="nomImage" class="help-inline"/>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-3 control-lable" for="portfolios">Portfolio</label>
					<div class="col-md-7">
						<select class="form-control input-sm" name="pId" id="pId">
							<c:if test="${not empty photo.portfolio}">
								<option value="${photo.portfolio.id}">${photo.portfolio.nomPortfolio}</option>
							</c:if>
							<c:forEach items="${portfolios}" var="portfolio">
								<option value="${portfolio.id}">${portfolio.nomPortfolio}</option>
							</c:forEach>
						</select>
						<div class="has-error">
							<form:errors path="portfolio" class="help-inline"/>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-3 control-lable" for="file">Fichier</label>
					<div class="col-md-7">
						<input type="file" name="file" class="form-control input-sm"/>
						<div class="has-error">
							<form:errors path="src" class="help-inline"/>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-actions floatRight">
					<c:choose>
						<c:when test="${edit}">
							<input type="submit" value="Enregistrer" class="btn btn-primary btn-sm"/> or <a href="<c:url value='/admin/photos' />">Annuler</a>
						</c:when>
						<c:otherwise>
							<input type="submit" value="Enregistrer" class="btn btn-primary btn-sm"/> or <a href="<c:url value='/admin/photos' />">Annuler</a>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</form:form>
	</div>
</body>
</html>