<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Administration | Picolia</title>
		<link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
		<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
	</head>
		<%@include file="authheader.jsp" %>	
		<div class="generic-container">
			<div class="panel panel-default">
				  <!-- Default panel contents -->
			  	<div class="panel-heading"><span class="lead">Administration</</span></div>
			  	<ul>
			  		<li><a href="<c:url value='/admin/users' />" >Gestion des utilisateurs</a></li>
			  		<li><a href="<c:url value='/admin/portfolios' />" >Gestion des portfolios</a></li>
			  		<li><a href="<c:url value='/admin/photos' />" >Gestion des photos</a></li>
			  		<li><a href="<c:url value='/admin/commentaires' />" >Gestion des commentaires</a></li>
			  	</ul>
			</div>
		</div>
	</body>
</html>