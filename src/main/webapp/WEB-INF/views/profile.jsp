<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Picolia | ${user.firstName} ${user.lastName}</title>
	<link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
	<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>

<body>
	<%@include file="authheader.jsp" %>	
	<div class="generic-container">
		<section>
            <div class="port-user">
                <div class="user-pic">
                    <img src="http://www.mariage.be/images/photographe-mariage.jpg" />
                    <div class="pic-shadow"></div>
                </div>
                <div class="user-infos">
                    <h2>
                    	<c:choose>
                    		<c:when test="${not empty user.firstName}">
                    			${user.firstName} ${user.lastName}
                    		</c:when>
                    		<c:otherwise>
                    			${user.username}
                    		</c:otherwise>
                   		</c:choose>
						<c:choose>
							<c:when test="${user.username eq currentuser}">
								<a href="<c:url value='/user/${currentuser}/edit' />" class="btn btn-success floatRight">Editer mon profil</a>
							</c:when>
						</c:choose>
                   	</h2>
                    <p>${user.description}</p>
                    <div class="networks">
                        <c:if test="${not empty user.facebookUrl}">
							<a href="${user.facebookUrl}" id="facebook"></a>
						</c:if>	
						<c:if test="${not empty user.twitterUrl}">
                        	<a href="${user.twitterUrl}" id="twitter"></a>
						</c:if>
						<c:if test="${not empty user.tumblrUrl}">
                        	<a href="${user.tumblrUrl}" id="tumblr"></a>
						</c:if>
						<c:if test="${not empty user.pinterestUrl}">
                        	<a href="${user.pinterestUrl}" id="linkedin"></a>
						</c:if>
						<c:if test="${not empty user.linkedinUrl}">
                        	<a href="${user.linkedinUrl}" id="pinterest"></a>
						</c:if>
						<c:if test="${not empty user.googleUrl}">
                        	<a href="${user.googleUrl}" id="google"></a>
						</c:if>
                    </div>
                </div>
            </div>
            <div class="square"></div>
        </section>
		<c:if test="${empty portfolios}">
		    Pas encore de portfolios.
		</c:if>
        <div class="port-content">
			<c:forEach items="${portfolios}" var="portfolio">
                <a href="<c:url value='/portfolio/${portfolio.id}' />">
		        	<div class="element" style="background:url('http://bgwall.net/wp-content/uploads/2014/09/eiffel-tower-landscape-tumblr-wallpaper.jpg');">
		                <div class="element-info">
	                        <br />
	                        <span class="elem-title">
	                            ${portfolio.nomPortfolio}
	                        </span>
		                </div>
		            </div>
                </a>
			</c:forEach>
		</div>
	</div>
</body>