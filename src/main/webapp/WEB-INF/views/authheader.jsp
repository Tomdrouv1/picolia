	<div class="authbar">
		<span class="logo">
			<a href="<c:url value="/" />">Picolia</a>
		</span>
		<span class="floatRight">
		<sec:authorize access="isAuthenticated()" >
			<c:choose>
				<c:when test="${not empty currentuser.username}">
					<a href="<c:url value="/user/${currentuser.username}" />"><img src="<c:url value="/static/images/profil.png" />" alt="portfolio"/></a>
				</c:when>
				<c:otherwise>
					<a href="<c:url value="/user/${user.username}" />"><img src="<c:url value="/static/images/profil.png" />" alt="portfolio"/></a>
				</c:otherwise>
			</c:choose>
				<a href="<c:url value="/portfolio/" />"><img src="<c:url value="/static/images/portfolio.png" />" alt="portfolio"/></a>
				<a href="<c:url value="/logout" />"><img src="<c:url value='/static/images/deco.png' />" alt="deconnexion"></a>
			</span>
		</sec:authorize>
	</div>