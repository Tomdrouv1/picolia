<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Portfolio ${portfolio.nomPortfolio}</title>
	<link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
	<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>

<body>
	<%@include file="authheader.jsp" %>	
	<div class="generic-container">
		<c:choose>
		   <c:when test="${empty portfolio}">
			<div class="generic-content">
		       Pas de portfolio pour cet id :(
	       	</div>
		   </c:when>
		   <c:otherwise>
		   <div class="generic-content">
			   	<div class="well lead">
			   		${portfolio.nomPortfolio} 
				   	<c:if test="${owner}">
				   		<div class="floatRight">
				   			<a href="<c:url value='/portfolio/delete/${portfolio.id}' />" class="btn btn-danger">Supprimer</a>
				   		</div>
				   	</c:if>
			   	</div>
			   	<h2>Photos</h2>
			   	<c:choose>
		   			<c:when test="${empty photos}">
       					Pas de photo pour ce portfolio :(
       					
			   			<c:choose>
						   	<c:when test="${owner}">
							<h2>Ajouter une photo</h2>
						   	<form:form action="${portfolio.id}/add?${_csrf.parameterName}=${_csrf.token}" method="POST" modelAttribute="photo" class="form-horizontal" command="photo" enctype="multipart/form-data">
								<form:input type="hidden" path="id" id="id"/>
								<div class="row"> 
									<div class="form-group col-md-12">
										<label class="col-md-3 control-lable" for="nomImage">Titre</label>
										<div class="col-md-7">
											<form:input type="text" path="nomImage" id="nomImage" class="form-control input-sm"/>
											<div class="has-error">
												<form:errors path="nomImage" class="help-inline"/>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-md-12">
										<label class="col-md-3 control-lable" for="file">Src</label>
										<div class="col-md-7">
											<input type="file" name="file" class="form-control input-sm"/>
											<div class="has-error">
												<form:errors path="src" class="help-inline"/>
											</div>
										</div>
									</div>
								</div>
			
								<div class="row">
									<div class="form-actions floatRight">
										<input type="submit" value="Valider" class="btn btn-primary btn-sm"/>
									</div>
								</div>
							</form:form>
						   	</c:when>
					   	</c:choose>
		   			</c:when>
	   				<c:otherwise>
					   	<ul class="last-portfolio">				
							<c:forEach items="${photos}" var="photo">
								<li>
									<a href="<c:url value='/photo/${photo.id}' />">
										<img src='<c:url value="/static/uploads/${photo.src}"></c:url>' />
									</a>
									<div class="title"><a href="<c:url value='/photo/${photo.id}' />">${photo.nomImage}</a></div>
								</li>
							</c:forEach>
						</ul>
			   			<c:choose>
						   	<c:when test="${owner}">
							<h2>Ajouter une photo</h2>
						   	<form:form action="${portfolio.id}/add?${_csrf.parameterName}=${_csrf.token}" method="POST" modelAttribute="photo" class="form-horizontal" command="photo" enctype="multipart/form-data">
								<form:input type="hidden" path="id" id="id"/>
								<div class="row"> 
									<div class="form-group col-md-12">
										<label class="col-md-3 control-lable" for="nomImage">Titre</label>
										<div class="col-md-7">
											<form:input type="text" path="nomImage" id="nomImage" class="form-control input-sm"/>
											<div class="has-error">
												<form:errors path="nomImage" class="help-inline"/>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-md-12">
										<label class="col-md-3 control-lable" for="file">Src</label>
										<div class="col-md-7">
											<input type="file" name="file" class="form-control input-sm"/>
											<div class="has-error">
												<form:errors path="src" class="help-inline"/>
											</div>
										</div>
									</div>
								</div>
			
								<div class="row">
									<div class="form-actions floatRight">
										<input type="submit" value="Valider" class="btn btn-primary btn-sm"/>
									</div>
								</div>
							</form:form>
						   	</c:when>
					   	</c:choose>
		   			</c:otherwise>
				</c:choose>
				</div>
			</div>
		   </c:otherwise>
		</c:choose>
	</div>
</body>