<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Picolia | ${user.username}</title>
	<link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
	<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>

<body>
	<%@include file="authheader.jsp" %>	
	<div class="generic-container">
		<div class="well lead">Editer mon profil</div>
		<form:form method="POST" modelAttribute="user" class="form-horizontal">
			<form:input type="hidden" path="id" id="id"/>
			
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-3 control-lable" for="firstName">Pr�nom</label>
					<div class="col-md-7">
						<form:input type="text" path="firstName" id="firstName" class="form-control input-sm"/>
						<div class="has-error">
							<form:errors path="firstName" class="help-inline"/>
						</div>
					</div>
				</div>
			</div>
	
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-3 control-lable" for="lastName">Nom</label>
					<div class="col-md-7">
						<form:input type="text" path="lastName" id="lastName" class="form-control input-sm" />
						<div class="has-error">
							<form:errors path="lastName" class="help-inline"/>
						</div>
					</div>
				</div>
			</div>
	
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-3 control-lable" for="username">Nom d'utilisateur</label>
					<div class="col-md-7">
						<c:choose>
							<c:when test="${edit}">
								<form:input type="text" path="username" id="username" class="form-control input-sm" disabled="true"/>
							</c:when>
							<c:otherwise>
								<form:input type="text" path="username" id="username" class="form-control input-sm" />
								<div class="has-error">
									<form:errors path="username" class="help-inline"/>
								</div>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-3 control-lable" for="facebookUrl">Url Facebook</label>
					<div class="col-md-7">
						<c:choose>
							<c:when test="${edit}">
								<form:input type="text" path="facebookUrl" id="facebookUrl" class="form-control input-sm"/>
							</c:when>
							<c:otherwise>
								<form:input type="text" path="facebookUrl" id="facebookUrl" class="form-control input-sm" />
								<div class="has-error">
									<form:errors path="facebookUrl" class="help-inline"/>
								</div>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-3 control-lable" for="googleUrl">Url Google+</label>
					<div class="col-md-7">
						<c:choose>
							<c:when test="${edit}">
								<form:input type="text" path="googleUrl" id="googleUrl" class="form-control input-sm"/>
							</c:when>
							<c:otherwise>
								<form:input type="text" path="googleUrl" id="googleUrl" class="form-control input-sm" />
								<div class="has-error">
									<form:errors path="googleUrl" class="help-inline"/>
								</div>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-3 control-lable" for="twitterUrl">Url Twitter</label>
					<div class="col-md-7">
						<c:choose>
							<c:when test="${edit}">
								<form:input type="text" path="twitterUrl" id="twitterUrl" class="form-control input-sm"/>
							</c:when>
							<c:otherwise>
								<form:input type="text" path="twitterUrl" id="twitterUrl" class="form-control input-sm" />
								<div class="has-error">
									<form:errors path="twitterUrl" class="help-inline"/>
								</div>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-3 control-lable" for="tumblrUrl">Url Tumblr</label>
					<div class="col-md-7">
						<c:choose>
							<c:when test="${edit}">
								<form:input type="text" path="tumblrUrl" id="tumblrUrl" class="form-control input-sm"/>
							</c:when>
							<c:otherwise>
								<form:input type="text" path="tumblrUrl" id="tumblrUrl" class="form-control input-sm" />
								<div class="has-error">
									<form:errors path="tumblrUrl" class="help-inline"/>
								</div>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-3 control-lable" for="pinterestUrl">Url Pinterest</label>
					<div class="col-md-7">
						<c:choose>
							<c:when test="${edit}">
								<form:input type="text" path="pinterestUrl" id="pinterestUrl" class="form-control input-sm"/>
							</c:when>
							<c:otherwise>
								<form:input type="text" path="pinterestUrl" id="pinterestUrl" class="form-control input-sm" />
								<div class="has-error">
									<form:errors path="pinterestUrl" class="help-inline"/>
								</div>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-3 control-lable" for="linkedinUrl">Url Linkedin</label>
					<div class="col-md-7">
						<c:choose>
							<c:when test="${edit}">
								<form:input type="text" path="linkedinUrl" id="linkedinUrl" class="form-control input-sm"/>
							</c:when>
							<c:otherwise>
								<form:input type="text" path="linkedinUrl" id="linkedinUrl" class="form-control input-sm" />
								<div class="has-error">
									<form:errors path="linkedinUrl" class="help-inline"/>
								</div>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
	
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-3 control-lable" for="password">Mot de passe</label>
					<div class="col-md-7">
						<form:input type="password" path="password" id="password" class="form-control input-sm" />
						<div class="has-error">
							<form:errors path="password" class="help-inline"/>
						</div>
					</div>
				</div>
			</div>
	
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-3 control-lable" for="email">Addresse mail</label>
					<div class="col-md-7">
						<form:input type="text" path="email" id="email" class="form-control input-sm" />
						<div class="has-error">
							<form:errors path="email" class="help-inline"/>
						</div>
					</div>
				</div>
			</div>
	
			<div class="row">
				<div class="form-actions floatRight">
					<c:choose>
						<c:when test="${edit}">
							<input type="submit" value="Enregistrer" class="btn btn-primary btn-sm"/> or <a href="<c:url value='/users' />">Annuler</a>
						</c:when>
						<c:otherwise>
							<input type="submit" value="Enregistrer" class="btn btn-primary btn-sm"/> or <a href="<c:url value='/users' />">Annuler</a>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</form:form>
	</div>
</body>