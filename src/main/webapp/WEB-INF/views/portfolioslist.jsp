<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Mes portfolios</title>
	<link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
	<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>

<body>
	<%@include file="authheader.jsp" %>	
	<div class="generic-container">
        <section>
            <div class="port-user perso">
                <div class="user-pic">
                    <img src="http://www.mariage.be/images/photographe-mariage.jpg" />
                    <div class="pic-shadow"></div>
                </div>
                <div class="user-infos">
                    <h2>
                   		Mes portfolios
                        
						<a href="<c:url value='/portfolio/new' />" class="btn btn-success floatRight">Ajouter un portfolio</a>
                    </h2>
                    
                </div>
            </div>
            <div class="square"></div>
        </section>
        <div class="port-content">
        	<c:forEach items="${portfolios}" var="portfolio">
              	<a href="<c:url value='/portfolio/${portfolio.id}' />">
	        		<div class="element" style="background:url('http://bgwall.net/wp-content/uploads/2014/09/eiffel-tower-landscape-tumblr-wallpaper.jpg');">
	                	<div class="element-info">
	                        <br />
	                        <span class="elem-title">
	                            ${portfolio.nomPortfolio}
	                        </span>
	                	</div>
	            	</div>
                 </a>
			</c:forEach>
         </div>		
	</div>
</body>