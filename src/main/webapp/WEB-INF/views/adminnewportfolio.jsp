<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Formulaire d'inscription</title>
	<link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
	<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>

<body>
	<%@include file="authheader.jsp" %>
 	<div class="generic-container">

		<div class="well lead">Formulaire portfolio</div>
	 	<form:form method="POST" modelAttribute="portfolio" class="form-horizontal">
			<form:input type="hidden" path="id" id="id"/>
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-3 control-lable" for="nomPortfolio">Nom portfolio</label>
					<div class="col-md-7">
						<form:input type="text" path="nomPortfolio" id="nomPortfolio" class="form-control input-sm"/>
						<div class="has-error">
							<form:errors path="nomPortfolio" class="help-inline"/>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-3 control-lable" for="userProfiles">Utilisateur</label>
					<div class="col-md-7">
						<select class="form-control input-sm" name="userId" id="userId">
							<c:if test="${not empty portfolio.user}"><option value="${portfolio.user.id}">${portfolio.user.username}</option></c:if>
							<c:forEach items="${users}" var="user">
								<option value="${user.id}">${user.username}</option>
							</c:forEach>
						</select>
						<div class="has-error">
							<form:errors path="user" class="help-inline"/>
						</div>
					</div>
				</div>
			</div>

	
			<div class="row">
				<div class="form-actions floatRight">
					<c:choose>
						<c:when test="${edit}">
							<input type="submit" value="Enregistrer" class="btn btn-primary btn-sm"/> or <a href="<c:url value='/admin/portfolios' />">Annuler</a>
						</c:when>
						<c:otherwise>
							<input type="submit" value="Enregistrer" class="btn btn-primary btn-sm"/> or <a href="<c:url value='/admin/portfolios' />">Annuler</a>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</form:form>
	</div>
</body>
</html>