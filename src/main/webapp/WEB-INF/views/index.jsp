<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Picolia</title>
	<link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
	<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>

<body>
	<%@include file="authheader.jsp" %>
	<div class="generic-container">
		<div class="well lead">Derniers portfolios</div>
		<div class="generic-content">
			<ul class="last-portfolio">
				<c:forEach items="${portfolios}" var="portfolio">
					<li>
						<a href="<c:url value='/portfolio/${portfolio.id}' />">
							<img src="http://bgwall.net/wp-content/uploads/2014/09/eiffel-tower-landscape-tumblr-wallpaper.jpg" />
						</a>
						<div class="title"><a href="<c:url value='/portfolio/${portfolio.id}' />">${portfolio.nomPortfolio} </a></div>
						<div>Par <a href="<c:url value='/user/${portfolio.user.username}' />">${portfolio.user.firstName} ${portfolio.user.lastName}</a></div>
					</li>
				</c:forEach>
			</ul>
			<a href="/picolia/portfolio/" class="btn btn-deafult btn-primary floatRight" >Mes portfolios</a>
		</div>
	</div>


</body>
</html>
