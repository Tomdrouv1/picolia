<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>AccessDenied page</title>
</head>
<body>
	<div class="generic-container">
		<div class="authbar">
			<span>Cher <strong>${loggedinuser}</strong>, vous n'�tes pas autoris� � acc�der � cette page.</span> <span class="floatRight"><a href="<c:url value="/logout" />"><img src="<c:url value='/static/images/deco.png' />" alt="deconnexion"></a></span>
		</div>
	</div>
</body>
</html>