<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Confirmation d'enregistrement</title>
	<link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
	<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>
<body>
	<%@include file="authheader.jsp" %>
	<div class="generic-container">
		
		<div class="alert alert-success lead">
	    	${success}
		</div>
		<span class="well floatRight">
			Retour � <a href="<c:url value='/users' />">la liste des utilisateurs</a>
		</span>
		<br /><br /><br />
	</div>
</body>

</html>