<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Inscription</title>
        <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
        <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
    </head>

    <body>
        <div class="register-container">
        	<div class="register-card">
            	<h2>Inscription</h2>
            	<div class="register-form">
		            <form:form method="POST" modelAttribute="user" class="form-horizontal" command="user">
		                <form:input type="hidden" path="id" id="id"/>
		                <div class="row">
		                    <div class="form-group col-md-12">
		                        <label class="col-md-3 control-lable" for="username">Nom d'utilisateur</label>
		                        <div class="col-md-9">
		                            <c:choose>
		                                <c:when test="${edit}">
		                                    <form:input type="text" path="username" id="username" class="form-control form-horizontal" disabled="true"/>
		                                </c:when>
		                                <c:otherwise>
		                                    <form:input type="text" path="username" id="username" class="form-control form-horizontal" />
		                                    <div class="has-error">
		                                        <form:errors path="username" class="help-inline"/>
		                                    </div>
		                                </c:otherwise>
		                            </c:choose>
		                        </div>
		                    </div>
		                </div>
		
		                <div class="row">
		                    <div class="form-group col-md-12">
		                        <label class="col-md-3 control-lable" for="password">Mot de passe</label>
		                        <div class="col-md-9">
		                            <form:input type="password" path="password" id="password" class="form-control form-horizontal" />
		                            <div class="has-error">
		                                <form:errors path="password" class="help-inline"/>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		
		                <div class="row">
		                    <div class="form-group col-md-12">
		                        <label class="col-md-3 control-lable" for="email">Adresse mail</label>
		                        <div class="col-md-9">
		                            <form:input type="text" path="email" id="email" class="form-control form-horizontal" />
		                            <div class="has-error">
		                                <form:errors path="email" class="help-inline"/>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                <br />
		                <div class="row">
		                    <div class="form-actions">
		                        <input type="submit" value="Enregistrer" class="btn btn-block btn-primary btn-default"/>
		                    </div>
		                </div>
	            </form:form>
           		<br />
                <div class="floatRight">
                   <a href="<c:url value="/login" />">Retour � la page d'accueil</a>
                </div>
	            </div>
            </div>
        </div>
    </body>
</html>
