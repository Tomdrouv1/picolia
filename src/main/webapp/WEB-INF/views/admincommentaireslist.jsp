<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Liste des commentaires</title>
	<link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
	<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>

<body>
	<%@include file="authheader.jsp" %>	
	<div class="generic-container">
		<div class="panel panel-default">
			  <!-- Default panel contents -->
		  	<div class="panel-heading"><span class="lead">Liste des commentaires</</span></div>
			<table class="table table-hover">
	    		<thead>
		      		<tr>
				        <th>Photo</th>
				        <th>Utilisateur</th>
				        <th>Contenu</th>
				        <th width="100"></th>  
					</tr>
		    	</thead>
	    		<tbody>
				<c:forEach items="${commentaires}" var="commentaire">
					<tr>
						<td>${commentaire.photo.nomImage}</td>
						<td>${commentaire.user.username}</td>
						<td>${commentaire.contenu}</td>
						<td><a href="<c:url value='/admin/commentaires/edit/${commentaire.id}' />" class="btn btn-success custom-width">Editer</a></td>
						<td><a href="<c:url value='/admin/commentaires/delete/${commentaire.id}' />" class="btn btn-danger custom-width">Supprimer</a></td>
					</tr>
				</c:forEach>
	    		</tbody>
	    	</table>
		</div>
	 	<div class="well">
	 		<a href="<c:url value='/admin/commentaires/new' />">Ajouter un commentaire</a>
	 	</div>
   	</div>
</body>
</html>