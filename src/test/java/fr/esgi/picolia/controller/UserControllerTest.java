package fr.esgi.picolia.controller;


import static org.mockito.Mockito.when;
import fr.esgi.picolia.model.User;
import fr.esgi.picolia.model.UserProfile;
import fr.esgi.picolia.service.UserProfileService;
import fr.esgi.picolia.service.UserService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.*;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

public class UserControllerTest {

    @Mock
    UserService userService;

    @Mock
    UserProfileService userProfileService;

    @Mock
    SecurityContext securityContext;

    @Mock
    Authentication authentication;

    @Mock
    AuthenticationTrustResolver authenticationTrustResolver;

    @Mock
    MessageSource messageSource;

    SecurityContextHolder securityContextHolder;

    @InjectMocks
    UserController userController;

    @Spy
    ModelMap model;

    @Spy
    List<User> users = new ArrayList<>();

    @Spy
    List<UserProfile> roles = new ArrayList<>();

    MockMvc mockMvc;

    Principal principal = new Principal() {
        @Override
        public String getName() {
            return "TEST_PRINCIPAL";
        }
    };

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        users = getUsersList();
        roles = getRoles();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        mockMvc = MockMvcBuilders.standaloneSetup(userController).setViewResolvers(viewResolver).build();
        securityContextHolder.setContext(securityContext);
    }

    @Test
    public void testUsersList() throws Exception {

        when(userService.findAllUsers()).thenReturn(users);
        when(userProfileService.findAll()).thenReturn(roles);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.getPrincipal()).thenReturn(principal);

        mockMvc.perform(MockMvcRequestBuilders.get("/users")
                .requestAttr("roles", "ADMIN")
                .principal(principal))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Test
    public void testLogin() throws Exception {

        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authenticationTrustResolver.isAnonymous(authentication)).thenReturn(false);

        mockMvc.perform(MockMvcRequestBuilders.get("/login")
                 .requestAttr("roles", "ADMIN")
                 .principal(principal))
                 .andExpect(MockMvcResultMatchers.status().is3xxRedirection());
    }

    @Test
    public void testRegisterPost() throws Exception {

        when(userService.isUsernameUnique(3, "test")).thenReturn(true);

        mockMvc.perform(MockMvcRequestBuilders.post("/register")
                 .requestAttr("roles", "ADMIN")
                 .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                 .param("id", "3")
                 .param("username", "test")
                 .param("email", "test@test.com")
                 .param("password", "test"))
                 .andExpect(MockMvcResultMatchers.status().isOk())
                 .andExpect(MockMvcResultMatchers.view().name("registersuccess"));
    }

    public List<User> getUsersList() {

        User user1 = new User();
        user1.setId(1);
        user1.setUsername("sam");
        user1.setFirstName("Sam");
        user1.setLastName("Smith");

        User user2 = new User();
        user2.setId(2);
        user2.setUsername("tomdrouv1");
        user2.setFirstName("Thomas");
        user2.setLastName("Drouvin");

        users.add(user1);
        users.add(user2);

        return users;
    }

    public List<UserProfile> getRoles() {
        UserProfile role1 = new UserProfile();
        role1.setType("ADMIN");

        roles.add(role1);

        return roles;
    }
}
