package fr.esgi.picolia.service;

import static org.junit.Assert.*;

import org.junit.Ignore;

import fr.esgi.picolia.dao.UserDao;

import fr.esgi.picolia.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    private UserDao userDao;
    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private UserServiceImpl userService;


    @Test
    public void testFindUserById() {

        Mockito.when(userDao.findById(1)).thenReturn(getExpectedUser());
        assertNotNull(userService.findById(1));
        assertEquals(getExpectedUser(), userService.findById(1));

    }

    private User getExpectedUser() {
        User user = new User();
        user.setUsername("sam");
        user.setFirstName("Sam");
        user.setLastName("Smith");
        user.setEmail("samy@xyz.com");
        return user;
    }
}
