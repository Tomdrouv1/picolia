package fr.esgi.picolia.dao;

import static org.junit.Assert.*;


import fr.esgi.picolia.model.Photo;

import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.joda.time.DateTime;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class PhotoDaoImplTest extends EntityDaoImpTest{
	
    @Autowired
    private PhotoDao photoDao;
	
    @Override
    protected IDataSet getDataSet() throws Exception {
        IDataSet[] dataSet = new IDataSet[] {
            new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("User.xml")),
            new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Portfolio.xml")),
            new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Photo.xml")),
            new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Commentaire.xml")),
            new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("UserProfile.xml")),
            new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("UserUserProfile.xml")),
        };
        return new CompositeDataSet(dataSet);
    }
    
    @Test
    public void testGetPhotos() {
        assertNotNull(photoDao.findAllPhotos());
    }
    
    @Test
    public void testGetPhotoById() {

        Photo actualPhoto = photoDao.findById(1);

        assertNotNull(actualPhoto);
        assertEquals(getExpectedPhoto().getDatePhoto(), actualPhoto.getDatePhoto()) ;
        assertEquals(getExpectedPhoto().getLikePhoto(), actualPhoto.getLikePhoto());
        assertEquals(getExpectedPhoto().getNomImage(), actualPhoto.getNomImage());
        assertEquals(getExpectedPhoto().getSrc(), actualPhoto.getSrc());
    }
    
    @Test
    public void testGetPhotoByName() {

        Photo actualPhoto = photoDao.findByNomImage("test");

        assertNotNull(actualPhoto);
        assertEquals(getExpectedPhoto().getDatePhoto(), actualPhoto.getDatePhoto()) ;
        assertEquals(getExpectedPhoto().getLikePhoto(), actualPhoto.getLikePhoto());
        assertEquals(getExpectedPhoto().getSrc(), actualPhoto.getSrc());
    }
    
    private Photo getExpectedPhoto() {
        Photo photo = new Photo();
        photo.setDatePhoto(new DateTime("2011-01-01").toDate());
        photo.setLikePhoto(0);
        photo.setNomImage("test");
        photo.setSrc("test.png");
        return photo;
    }
}
