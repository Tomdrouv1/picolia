package fr.esgi.picolia.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


import fr.esgi.picolia.model.Portfolio;
import fr.esgi.picolia.model.User;

import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class PortfolioDaoImplTest extends EntityDaoImpTest {
	
    @Autowired
    private PortfolioDao portfolioDao;

    @Autowired
    private UserDao userDao;
	
    @Override
    protected IDataSet getDataSet() throws Exception {
        IDataSet[] dataSet = new IDataSet[] {
            new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("User.xml")),
            new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Portfolio.xml")),
            new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Photo.xml")),
            new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Commentaire.xml")),
            new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("UserProfile.xml")),
            new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("UserUserProfile.xml")),
        };
        return new CompositeDataSet(dataSet);
    }
    
    @Test
    public void testGetPortfolios() {
        assertNotNull(portfolioDao.findAllPortfolios());
    }
    
    @Test
    public void testGetPortfolioById() {

        Portfolio actualPortfolio = portfolioDao.findById(1);

        assertNotNull(actualPortfolio);
        assertEquals(getExpectedPortfolio().getLikePortfolio(), actualPortfolio.getLikePortfolio()) ;
        assertEquals(getExpectedPortfolio().getNomPortfolio(), actualPortfolio.getNomPortfolio());
    }
    
    @Test
    public void testGetPortfolioByName() {

        Portfolio actualPortfolio = portfolioDao.findByNomPortfolio("test");

        assertNotNull(actualPortfolio);
        assertEquals(getExpectedPortfolio().getLikePortfolio(), actualPortfolio.getLikePortfolio()) ;
    }
    
    private Portfolio getExpectedPortfolio() {
        Portfolio portfolio = new Portfolio();
        portfolio.setLikePortfolio(0);
        portfolio.setNomPortfolio("test");
        return portfolio;
    }
    
    private User getExpectedUser() {
        User user = new User();
        user.setUsername("sam");
        user.setFirstName("Sam");
        user.setLastName("Smith");
        user.setEmail("samy@xyz.com");
        user.setPassword("test");
        return user;
    }

}
