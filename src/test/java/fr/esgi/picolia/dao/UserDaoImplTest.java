package fr.esgi.picolia.dao;

import static org.junit.Assert.*;

import fr.esgi.picolia.model.User;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class UserDaoImplTest extends EntityDaoImpTest{

    @Autowired
    private UserDao userDao;

   @Override
   protected IDataSet getDataSet() throws Exception {
       IDataSet[] dataSet = new IDataSet[] {
           new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("User.xml")),
           new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Portfolio.xml")),
           new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Photo.xml")),
           new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Commentaire.xml")),
           new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("UserProfile.xml")),
           new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("UserUserProfile.xml")),
       };
       return new CompositeDataSet(dataSet);
   }

    @Test
    public void testGetUsers() {

        assertNotNull(userDao.findAllUsers());
    }

    @Test
    public void testGetUserById() {

        User actualUser = userDao.findById(1);

        assertNotNull(actualUser);
        assertEquals(getExpectedUser().getFirstName(), actualUser.getFirstName()) ;
        assertEquals(getExpectedUser().getLastName(), actualUser.getLastName());
        assertEquals(getExpectedUser().getUsername(), actualUser.getUsername());
        assertEquals(getExpectedUser().getEmail(), actualUser.getEmail());
    }

    @Test
    public void testGetUserByUsername() {

        User actualUser = userDao.findByUsername("sam");

        assertNotNull(actualUser);
        assertEquals(getExpectedUser().getLastName(), actualUser.getLastName());
        assertEquals(getExpectedUser().getUsername(), actualUser.getUsername());
        assertEquals(getExpectedUser().getEmail(), actualUser.getEmail());
    }

    private User getExpectedUser() {
        User user = new User();
        user.setUsername("sam");
        user.setFirstName("Sam");
        user.setLastName("Smith");
        user.setEmail("samy@xyz.com");
        return user;
    }
}
