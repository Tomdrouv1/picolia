package fr.esgi.picolia.dao;

import static org.junit.Assert.*;


import fr.esgi.picolia.model.Commentaire;
import fr.esgi.picolia.model.Photo;
import fr.esgi.picolia.model.User;

import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.joda.time.DateTime;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class CommentaireDaoImplTest extends EntityDaoImpTest{
	
    @Autowired
    private CommentaireDao commentaireDao;

   @Override
   protected IDataSet getDataSet() throws Exception {
       IDataSet[] dataSet = new IDataSet[] {
           new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("User.xml")),
           new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Portfolio.xml")),
           new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Photo.xml")),
           new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Commentaire.xml")),
           new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("UserProfile.xml")),
           new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("UserUserProfile.xml")),
       };
       return new CompositeDataSet(dataSet);
   }

    @Test
    public void testGetCommentaires() {

        assertNotNull(commentaireDao.findAllCommentaires());
    }
    
    @Test
    public void testGetCommentaireById() {

        Commentaire actualCommentaire = commentaireDao.findById(1);

        assertNotNull(actualCommentaire);
        assertEquals(getExpectedCommentaire().getDateCom(), actualCommentaire.getDateCom()) ;
        assertEquals(getExpectedCommentaire().getContenu(), actualCommentaire.getContenu());
        assertEquals(getExpectedCommentaire().getPhoto().getSrc(), actualCommentaire.getPhoto().getSrc());
        assertEquals(getExpectedCommentaire().getUser().getUsername(), actualCommentaire.getUser().getUsername());
    }
    
    private Commentaire getExpectedCommentaire() {
    	Photo photo = getExpectedPhoto();
    	User user = getExpectedUser();
    	
        Commentaire commentaire = new Commentaire();
        commentaire.setDateCom(new DateTime("2016-07-18").toDate());
        commentaire.setContenu("Blabla");
        commentaire.setPhoto(photo);
        commentaire.setUser(user);
        return commentaire;
    }
    
    private User getExpectedUser() {
        User user = new User();
        user.setId(1);
        user.setUsername("sam");
        return user;
    }
    
    private Photo getExpectedPhoto() {
        Photo photo = new Photo();
        photo.setId(1);
        photo.setDatePhoto(new DateTime("2011-01-01").toDate());
        photo.setSrc("test.png");
        photo.setNomImage("test");
        photo.setLikePhoto(0);
        return photo;
    }

}
