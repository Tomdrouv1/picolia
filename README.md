# PICOLIA #

## Comment récupérer le dossier? ##

```
#!bash

git clone git@bitbucket.org:Tomdrouv1/picolia.git

```

## J'ai récupéré le dossier, que dois-je faire maintenant ? ##

Une fois le dépôt récupéré : 

- Vérifiez si java est bien installé et que le chemin vers le dossier bin/ de votre JDK est bien dans le PATH en tapant la commande suivante :


```
#!bash

java -version

```

Si vous avez une réponse du genre :

```
#!bash

java version "1.8.0_91"
Java(TM) SE Runtime Environment (build 1.8.0_91-b14)
Java HotSpot(TM) 64-Bit Server VM (build 25.91-b14, mixed mode)

```

Java est bien installé!

Sinon téléchargez le **JDK 1.8** (ne pas prendre le JRE) sur le site d'Oracle http://www.oracle.com/technetwork/java/javase/downloads et installez le.


- Vérifiez si la commande de Maven est disponible sur votre machine :

```
#!bash

mvn -v

```
Si vous avez une réponse du genre :

```
#!bash

Apache Maven 3.3.9 (bb52d8502b132ec0a5a3f4c09453c07478323dc5; 2015-11-10T17:41:47+01:00)
Maven home: C:\Maven\apache-maven-3.3.9
Java version: 1.8.0_74, vendor: Oracle Corporation
Java home: C:\Program Files\Java\jdk1.8.0_74\jre
Default locale: fr_FR, platform encoding: Cp1252
OS name: "windows 10", version: "10.0", arch: "amd64", family: "dos"

```
Maven est bien installé!

Sinon téléchargez le dossier compressé comprenant les **binaries** sur https://maven.apache.org/download.cgi et décompressez le où vous voulez (à la racine de votre disque dur par exemple).
Ceci étant fait, modifiez vos variables d'environnement pour ajouter le chemin faire le dossier bin/ de Maven.
Enfin vérifiez que tout est bon en utilisant la commande écrite plus haut.

Une fois ces étapes passées avec succès, ouvrez une console à la racine du projet et lancez la commande suivante :

```
#!bash

mvn clean install

```

Cela téléchargera toutes les bibliothèques requises par le projet.

## Déploiement sur un serveur local Tomcat ##

Téléchargez le dossier compressé ici : http://tomcat.apache.org/download-80.cgi ou le fichier .exe et installez tomcat où vous le souhaitez.

Selon votre IDE vous pouvez suivre les instructions suivantes (Eclipse et IntelliJ)

### Eclipse ###

Si Eclipse n'est pas installé sur votre machine, téléchargez le ici : http://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/mars2.
Ouvrez Eclipse.

Dans l'onglet *"Window"*, sélectionnez *"Show View" => "Other..."* puis recherchez *"Server"*.
Un onglet s'ouvre en bas de l'IDE. Cliquez sur le lien permettant de créer un nouveau serveur et une fenêtre s'ouvrira.
Sélectionnez le dossier Apache puis Tomcat v8.0 Server, cliquez sur Suivant et utilisez le bouton *"Browse"* pour indiquer le chemin vers le dossier racine de Tomcat installé préalablement ainsi que le bouton *"Installed JREs"* pour indiquer le chemin vers le dossier jre/ du JDK installé plus tôt (et non du JRE qui peut être sélectionné par défaut).

Il est préférable de s'assurer que les librairies ont bien toutes été chargées en faisant un clic droit sur le dossier racine, sélectionner *"Run as.." => "maven clean"* puis clic droit une nouvelle fois au même endroit et choisir *"Run as.." => "maven install"*.

### IntelliJ ###

Si IntelliJ n'est pas installé sur votre machine, téléchargez le ici : https://www.jetbrains.com/idea/?fromMenu#chooseYourEdition.
Ouvrez IntelliJ.

Dans l'onglet *"Run"*, sélectionnez *"Edit configurations.."*.
Dans la nouvelle fenêtre cliquez sur le symbole **+** vert en haut à gauche de celle-ci et choisissez *"Tomcat Server" => "Local"*. Si cet option n'apparaît pas cliquez sur *"XX items mores (irrelevant)"* et vous trouverez parmi la liste complète *"Tomcat Server"*.
L'onglet server est ouvert en premier. Dans celui-ci il vous faut configurer **Tomcat**, pour ce faire cliquez sur *"Configure.."* puis sur le symbole **+** vert en haut à gauche de la nouvelle fenêtre (il est possible qu'une fenêtre différente s'ouvre ne comportant pas le symbole et appelée *Tomcat Server*, cela correspond à l'étape qui suit donc ne vous inquiétez pas).
Dans le champ *Tomcat Home* sélectionner le dossier racine de Tomcat installé quelques étapes avant.
Ensuite il faut préciser le JRE à utiliser (ici 1.8) et cliquer sur *"OK"*.

Une fenêtre s'ouvre alors en bas de votre IDE représentant les serveurs configurés (le Tomcat doit être le seul si c'est la première fois que vous faites cela). Faites un clic droit sur votre nouveau serveur local et choisissez *"Artifacts"*. Vous devriez voir 2 war de votre projet, un normal et un exploded (qui est le même mais décompressé). Sélectionnez le premier puis faites *"Add"* et enfin *"OK"*.
Il ne reste plus qu'a cliquer sur la flèche verte de la fenêtre des serveurs pour démarrer Tomcat.

## Et une fois la configuration terminée ! ##
Pour les deux IDE, si vous n'avez pas changé les paramètres par défaut, votre projet devrait être accessible à l'adresse suivante : http://localhost:8080/

Si vous rencontrez le moindre problème, contactez le chef de projet ou tout autre personne qui pourrait vous aider.

#Merci pour la lecture et bon projet!#